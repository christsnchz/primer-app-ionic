import { Component } from '@angular/core';
import { DeseosService } from 'src/app/servicios/deseos.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Lista } from 'src/app/modelos/lista.model';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor( public deseosService: DeseosService, 
    private router: Router , private alertCtrl: AlertController) {

  }

 async agregarLista() {
   //
    
    const alert = await this.alertCtrl.create({
      header: 'Nueva lista',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'nombre de la lista'
        }
      ],
      buttons:[
        {
          text: 'Cancelar',
          role: 'cancel',          
        },
        {
          text:'Crear',
          handler: (data) => {
            if(data.titulo.length === 0)
            {
              return;
            }
            const listid = this.deseosService.crearLista(data.titulo);   
            
            this.router.navigateByUrl(`/tabs/tab1/agregar/${listid}`);
          }
        }
      ]
    });

    alert.present();

  }



}
